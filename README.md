# SaaS Operations Dictionary
This SaaS Operations Dictionary is for Site Reliability Engineers (SRE's), Cloud Architects, DevOps engineers, SaaS Operations engineers and QA automation engineers.

We began creating this content so that we could better understand the terminology used to measure performance of SaaS applications. Feel free to contribute but first visit our "Contribution Guidelines".

## Table of Contents
- [Code of Conduct](..blob/master/CODE_OF_CONDUCT.md)
- [Contributing Guidelines](..blob/master/contributing.md)
- Dictionary
  - Word1
  - Word2
  - Word3


## Dictionary

| Term | Definition | Reference |
| ------------- | ------------- | ----- |
| Service Level Agreement (SLA) | An explicit or implicit contract with your users that includes consequences of meeting (or missing) the Service Level Objectives (SLOs) they contain. The consequences of not meeting an SLO identified in an SLA are usually financial but can be defined clearly in the SLA.  | https://landing.google.com/sre/book/chapters/service-level-objectives.html |
| Service Level Indicator (SLI) | A carefully defined quantitative measure of some aspect of the level of service that is provided. Common SLI's include request latency, error rate, system throughput, availability and yield.  | https://landing.google.com/sre/book/chapters/service-level-objectives.html |
